package com.example.projectchat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.projectchat.model.Chat;

import java.util.ArrayList;
import java.util.List;

class Rc_adpater extends RecyclerView.Adapter<Rc_adpater.ViewHolder> {

    int MSG_RIGHT=1;
    List<Chat> chats;
    String email;

    public Rc_adpater(List<Chat> chats, String email) {
        this.chats = chats;
        this.email = email;
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        ImageView img;
        TextView name,message;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img=itemView.findViewById(R.id.img_message);
            name=itemView.findViewById(R.id.nameuser);
            message=itemView.findViewById(R.id.chat_message);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == MSG_RIGHT){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycleview2,parent,false);
            return new ViewHolder(v);
        }else{
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycleview,parent,false);
            return new ViewHolder(v);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.message.setText(chats.get(position).getMessage());
        holder.name.setText(chats.get(position).getName());
        if (chats.get(position).getImgurl()==null){
            holder.img.setVisibility(View.GONE);
        }else {
            Glide.with(holder.img.getContext()).load(chats.get(position).getImgurl()).into(holder.img);
        }
    }

    @Override
    public int getItemCount() {
        return chats.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (chats.get(position).getEmail() == email){
            return 0;
        }else {
            return 1;
        }
    }
}
